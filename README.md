A work-in-progress Game Boy Color emulator written in Rust.

Implements the GBC CPU instruction set specified in the [Game Boy Programming Manual](http://chrisantonellis.com/files/gameboy/gb-programming-manual.pdf).
