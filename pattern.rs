use std::env;

fn main() {
	let args: Vec<String> = env::args().collect();
	if args.len() != 2 {
		println!("usage: pattern str");
		println!("example of str: 01aaabbb");
		println!("parameters must be of lenth 3, i.e. aaa, bbb");
		return;
	}
	generate(&args[1]);
}

// example form: 01aaabbb; a must always come before b
fn generate(pattern: &str) {
	if pattern.len() != 8 {
		println!("pattern not 8 bits");
		return;
	}
	let params =
		if pattern.contains("a") && pattern.contains("b") {
			2
		} else if pattern.contains("a") {
			1
		} else {
			0
		};
	let reg = vec!("000", "001", "010", "011", "100", "101", "111");
	
	if params == 0 {
		println!("{:#02x}", u8::from_str_radix(pattern, 2).unwrap());
		return
	}
	if params == 1 {
		if !pattern.contains("aaa") {
			println!("pattern with 1 parameter doesn't contain aaa");
			return
		}
		let mut prev = u8::from_str_radix(&pattern.replace("aaa", "000"), 2).unwrap();
		let mut range_start = prev; // keep track of number at the start of the range
		print!("{:#02x}", prev);
		for a in &reg {
			let opcode = u8::from_str_radix(&pattern.replace("aaa", a), 2).unwrap();
			if opcode - prev > 1 { // if difference > 1, end current range
				if range_start != prev {
					print!("...{:#02x} | ", prev); // "{:#010b}" for binary formatting
				} else { // to prevent something like 0x00...0x00 |; makes it 0x00 | instead
					print!(" | ");
				}
				print!("{:#02x}", opcode); // start new range
				range_start = opcode;
				prev = opcode
			} else {
				prev = opcode
			}
		}
		println!("");
		return
	}
	if params == 2 {
		if !(pattern.contains("aaa") && pattern.contains("bbb")) {
			println!("pattern with 2 parameters doesn't contain aaa and bbb");
			return
		}
		let replace_a = pattern.replace("aaa", "000");
		let mut prev = u8::from_str_radix(&replace_a.replace("bbb", "000"), 2).unwrap();
		let mut range_start = prev;
		print!("{:#02x}", prev);
		for a in &reg {
			for b in &reg {
				let opcode_replace_a = pattern.replace("aaa", a);
				let opcode = u8::from_str_radix(&opcode_replace_a.replace("bbb", b), 2).unwrap();
				if opcode - prev > 1 {
					if range_start != prev {
						print!("...{:#02x} | ", prev); // "{:#010b}" for binary formatting
					} else {
						print!(" | ");
					}
					print!("{:#02x}", opcode);
					range_start = opcode;
					prev = opcode
				} else {
					prev = opcode
				}
			}
		}
		println!("");
		return
	}
}