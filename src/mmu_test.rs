#[cfg(test)]
use mmu::Mmu;

#[test]
fn read_byte() {
    let mut mmu = Mmu::new();
    let (location, data) = (0xabcd, 0xef);
    mmu.buf[location] = data;
    assert!(mmu.read_byte(location as u16) == data);
}

#[test]
fn read_word() {
    let mut mmu = Mmu::new();
    let (location, lsb, msb, data) = (0xabcd, 0xef, 0x78, 0x78ef);
    mmu.buf[location] = lsb;
    mmu.buf[location + 1] = msb;
    assert!(mmu.read_word(location as u16) == data);
}

#[test]
fn write_byte() {
    let mut mmu = Mmu::new();
    let (location, data) = (0xabcd, 0xef);
    mmu.write_byte(location as u16, data);
    assert!(mmu.buf[location] == data);
}

#[test]
fn write_word() {
    let mut mmu = Mmu::new();
    let (location, lsb, msb, data) = (0xabcd, 0xef, 0x78, 0x78ef);
    mmu.write_word(location as u16, data);
    assert!(mmu.read_byte(location as u16) == lsb);
    assert!(mmu.read_byte(location + 1 as u16) == msb);
}