//! Memory management unit implementation.

pub struct Mmu {
    pub buf: [u8; 0xffff + 1]
}

impl Mmu {
    pub fn new() -> Mmu {
        Mmu {
            buf: [1; 0xffff + 1]
        }
    }

    pub fn read_byte(&self, addr: u16) -> u8 {
        self.buf[addr as usize]
    }

    pub fn read_word(&self, addr: u16) -> u16 {
        let lsb = self.read_byte(addr); // little endian
        let msb = self.read_byte(addr + 1);
        ((msb as u16) << 8) | ((lsb as u16) & 0xff)
    }

    pub fn write_byte(&mut self, addr: u16, val: u8) {
        self.buf[addr as usize] = val
    }

    pub fn write_word(&mut self, addr: u16, val: u16) {
        let lsb = (val & 0xff) as u8; // little endian
        let msb = (val >> 8) as u8;
        self.buf[addr as usize] = lsb;
        self.buf[(addr + 1) as usize] = msb;
    }
}
