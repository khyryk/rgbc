#[cfg(test)]
use cpu::{Cpu, REG_A, REG_B, REG_C, REG_D, REG_E, REG_H, REG_L};
#[cfg(test)]
use mmu::Mmu;

// LD r,n : Loads 8-bit immediate data into register r.
// 00 r 110
#[test]
fn ld_r_n() {
    let (opcode, data) = (0b00_111_110, 111);
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    cpu.mmu.buf[0] = opcode;
    cpu.mmu.buf[1] = data;
    cpu.execute(opcode);
    assert!(cpu.get_register(REG_A) == data);
}

// LD r, r' : Loads the contents of register r' into register r.
// 01 r r'
#[test]
fn ld_r_rp() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let data = 15;
    cpu.set_register(REG_C, data);
    let opcode = 0b01_111_001;
    cpu.execute(opcode);
    assert!(cpu.get_register(REG_A) == cpu.get_register(REG_C));
}

// LD r, (HL) : Loads the contents of memory (8 bits) specified by
// register pair HL into register r.
// 01 r 110
#[test]
fn ld_r_hl() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let opcode = 0b01_000_110;
    let location = 0xf00f;
    cpu.set_register(REG_H, 0xf0);
    cpu.set_register(REG_L, 0x0f);
    cpu.mmu.write_byte(location, 17);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_B));
}

// LD (HL), r : Store the contents of register r in memory specified by register pair HL.
// 01 110 r
#[test]
fn ld_hl_r() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let opcode = 0b01_110_000;
    let location = 0xf00f;
    cpu.set_register(REG_H, 0xf0);
    cpu.set_register(REG_L, 0x0f);
    cpu.set_register(REG_B, 55);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_B));
}

// LD (HL), n : Loads 8-bit immediate data n into memory specified by register pair HL.
// 00 110 110
#[test]
fn ld_hl_n() {
    let (opcode, data) = (0b00_110_110, 211);
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    cpu.mmu.buf[0] = opcode;
    cpu.mmu.buf[1] = data;
    let location = 0xf00f;
    cpu.set_register(REG_H, 0xf0);
    cpu.set_register(REG_L, 0x0f);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == data);
}

// LD A, (BC) : Loads the contents specified by the contents of register pair BC into register A.
// 00 001 010
#[test]
fn ld_a_bc() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let opcode = 0b00_001_010;
    let location = 0xf00f;
    cpu.set_register(REG_B, 0xf0);
    cpu.set_register(REG_C, 0x0f);
    cpu.mmu.write_byte(location, 42);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
}

// LD A, (DE) : Loads the contents specified by the contents of register pair DE into register A.
// 00 011 010
#[test]
fn ld_a_de() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let opcode = 0b00_011_010;
    let location = 0xf00f;
    cpu.set_register(REG_D, 0xf0);
    cpu.set_register(REG_E, 0x0f);
    cpu.mmu.write_byte(location, 1);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
}

// LD A, (C) : Loads into register A the contents of the internal RAM, port register,
// or mode register at the address in the range 0xFF00-0xFFFF specified by register C.
// (E.g., C is the LSB; MSB = 0xFF.)
// 11 110 010
#[test]
fn ld_a_c() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let opcode = 0b11_110_010;
    let location = 0xffee;
    cpu.set_register(REG_C, 0xee);
    cpu.mmu.write_byte(location, 77);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
}

#[test]
fn ld_a_c_2() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let opcode = 0b11_110_010;
    let location = 0xff95;
    cpu.set_register(REG_C, 0x95);
    cpu.mmu.write_byte(location, 78);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
}

// LD (C), A : Loads the contents of register A in the internal RAM, port register,
// or mode register at the address in the range 0xFF00-0xFFFF specified by register C.
// 11 100 010
#[test]
fn ld_c_a() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let opcode = 0b11_100_010;
    let location = 0xff9f;
    cpu.set_register(REG_C, 0x9f);
    cpu.mmu.write_byte(location, 71);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
}

// LD A, (n) : Loads into register A the contents of the internal RAM, port register,
// or mode register at the address in the range 0xFF00-0xFFFF specified by the
// 8-bit immediate operand n.
// 11 110 000
#[test]
fn ld_a_n() {
    let (opcode, data) = (0b11_110_000, 123);
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    cpu.mmu.buf[0] = opcode;
    cpu.mmu.buf[1] = data;
    let location = 0xff00 + (data as u16);
    cpu.mmu.write_byte(location, 18);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
}

// LD (n), A : Loads the contents of register A to the internal RAM, port register,
// or mode register at the address in the range 0xFF00-0xFFFF specified by the
// 8-bit immediate operand n.
// 11 100 000
#[test]
fn ld_n_a() {
    let (opcode, data) = (0b11_100_000, 223);
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    cpu.mmu.buf[0] = opcode;
    cpu.mmu.buf[1] = data;
    let location = 0xff00 + (data as u16);
    cpu.set_register(REG_A, 177);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
}

// LD A, (nn) : Loads into register A the contents of the internal RAM or register
// specified by the 16-bit immediate operand nn.
// 11 111 010
#[test]
fn ld_a_nn() {
    let (opcode, data1, data2) = (0b11_111_010, 0xcd, 0xab);
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    cpu.mmu.buf[0] = opcode;
    cpu.mmu.buf[1] = data1;
    cpu.mmu.buf[2] = data2;
    let location = 0xabcd;
    cpu.mmu.write_byte(location, 18);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
}

// LD (nn), A : Loads the contents of register A to the internal RAM or register
// specified by the 16-bit immediate operand nn.
// 11 101 010
#[test]
fn ld_nn_a() {
    let (opcode, data1, data2) = (0b11_101_010, 0x7f, 0x1a);
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    cpu.mmu.buf[0] = opcode;
    cpu.mmu.buf[1] = data1;
    cpu.mmu.buf[2] = data2;
    let location = 0x1a7f;
    cpu.set_register(REG_A, 187);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
}

// LD A, (HLI) : Loads in register A the contents of memory specified by the contents
// of register pair HL and simultaneously increments the contents of HL.
// 00 101 010
#[test]
fn ld_a_hli() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let opcode = 0b00_101_010;
    let location = 0xf00f;
    cpu.set_register(REG_H, 0xf0);
    cpu.set_register(REG_L, 0x0f);
    cpu.mmu.write_byte(location, 71);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
    let msb = cpu.get_register(REG_H);
    let lsb = cpu.get_register(REG_L);
    assert!((location + 1) as u16 == ((msb as u16) << 8) | ((lsb as u16) & 0xff));
}

// LD A, (HLD) : Loads in register A the contents of memory specified by the contents
// of register pair HL and simultaneously decrements the contents of HL.
// 00 111 010
#[test]
fn ld_a_hld() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let opcode = 0b00_111_010;
    let location = 0xf12f;
    cpu.set_register(REG_H, 0xf1);
    cpu.set_register(REG_L, 0x2f);
    cpu.mmu.write_byte(location, 72);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
    let msb = cpu.get_register(REG_H);
    let lsb = cpu.get_register(REG_L);
    assert!((location - 1) as u16 == ((msb as u16) << 8) | ((lsb as u16) & 0xff));
}

// LD (BC), A : Store the contents of register A in the memory specified by register pair BC.
// 00 000 010
#[test]
fn ld_bc_a() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let opcode = 0b00_000_010;
    let location = 0x108f;
    cpu.set_register(REG_B, 0x10);
    cpu.set_register(REG_C, 0x8f);
    cpu.mmu.write_byte(location, 11);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
}

// LD (DE), A : Store the contents of register A in the memory specified by register pair DE.
// 00 010 010
#[test]
fn ld_de_a() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let opcode = 0b00_010_010;
    let location = 0x197f;
    cpu.set_register(REG_D, 0x19);
    cpu.set_register(REG_E, 0x7f);
    cpu.mmu.write_byte(location, 111);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
}

// LD (HLI), A : Stores the contents of register A in the memory specified by register pair HL
// and simultaneously increments the contents of HL.
// 00 100 010
#[test]
fn ld_hli_a() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let opcode = 0b00_100_010;
    let location = 0x0ff0;
    cpu.set_register(REG_H, 0x0f);
    cpu.set_register(REG_L, 0xf0);
    cpu.set_register(REG_A, 70);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
    let msb = cpu.get_register(REG_H);
    let lsb = cpu.get_register(REG_L);
    assert!((location + 1) as u16 == ((msb as u16) << 8) | ((lsb as u16) & 0xff));
}

// LD (HLD), A : Stores the contents of register A in the memory specified by register pair HL
// and simultaneously decrements the contents of HL.
// 00 110 010
#[test]
fn ld_hld_a() {
    let mut mmu = Mmu::new();
    let mut cpu = Cpu::new(&mut mmu);
    let opcode = 0b00_110_010;
    let location = 0xffff;
    cpu.set_register(REG_H, 0xff);
    cpu.set_register(REG_L, 0xff);
    cpu.set_register(REG_A, 7);
    cpu.execute(opcode);
    assert!(cpu.mmu.read_byte(location) == cpu.get_register(REG_A));
    let msb = cpu.get_register(REG_H);
    let lsb = cpu.get_register(REG_L);
    assert!((location - 1) as u16 == ((msb as u16) << 8) | ((lsb as u16) & 0xff));
}
