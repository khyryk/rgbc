//! Game Boy Color emulator.

extern crate sdl2;
use sdl2::cpuinfo::cpu_count;
pub mod cpu;
pub mod mmu;
mod cpu_test;
mod mmu_test;

fn main() {
    println!("cpu count: {}", cpu_count());

    let mut mmu = mmu::Mmu::new();
    let mut cpu = cpu::Cpu::new(&mut mmu);
    cpu.execute(0b01000001);
    /*
    loop {
        let instruction = cpu.fetch();
        let cycles = cpu.execute(instruction);
    }
    */
}
