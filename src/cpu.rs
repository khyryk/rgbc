//! Implements the GBC CPU instruction set specified in the
//! [GB Programming Manual](http://chrisantonellis.com/files/gameboy/gb-programming-manual.pdf).

use mmu::Mmu;

pub const REG_A: u8 = 0b111;
pub const REG_B: u8 = 0b000;
pub const REG_C: u8 = 0b001;
pub const REG_D: u8 = 0b010;
pub const REG_E: u8 = 0b011;
pub const REG_H: u8 = 0b100;
pub const REG_L: u8 = 0b101;

#[allow(non_snake_case)]
#[allow(dead_code)]
///
/// # Examples
///
/// ```
/// // execute a single instruction
/// let mut mmu = mmu::Mmu::new();
/// let mut cpu = cpu::Cpu::new(&mut mmu);
/// cpu.execute(0b01_000_001);
/// ```
///
/// ```
/// // execute a program loaded into the MMU
/// let mut cpu = cpu::Cpu::new(&mut mmu);
/// loop { // fetch-decode-execute cycle
///     let instruction = cpu.fetch();
///     let cycles = cpu.execute(instruction);
/// }
/// ```
///
pub struct Cpu<'a> {
    B: u8,
    C: u8,
    D: u8,
    E: u8,
    H: u8,
    L: u8,
    A: u8, // accumulator; stores results of arithmetic and logical operations
    F: u8, // flags (GameBoy CPU Manual p. 62)
    // Z (zero) N (substract) H (half carry) C (carry) 0 0 0 0
    // pairs: AF, BC, DE, HL
    SP: u16,
    PC: u16,
    pub mmu: &'a mut Mmu,
}

impl<'a> Cpu<'a> {
    /// Initializes registers and stores reference to MMU.
    pub fn new(mmu: &'a mut Mmu) -> Cpu<'a> {
        Cpu {
            B: 0,
            C: 0,
            D: 0,
            E: 0,
            H: 0,
            L: 0,
            A: 0,
            F: 0,
            SP: 0xFFFE,
            PC: 0,
            mmu: mmu,
        }
    }

    /// Returns the instruction at address specified by PC.
    pub fn fetch(&self) -> u8 {
        self.mmu.read_byte(self.PC)
    }

    pub fn get_register(&self, reg: u8) -> u8 {
        match reg {
            REG_A => self.A,
            REG_B => self.B,
            REG_C => self.C,
            REG_D => self.D,
            REG_E => self.E,
            REG_H => self.H,
            REG_L => self.L,
            _ => panic!("No such register exists: {:#010b}", reg),
        }
    }

    pub fn set_register(&mut self, reg: u8, data: u8) {
        match reg {
            REG_A => self.A = data,
            REG_B => self.B = data,
            REG_C => self.C = data,
            REG_D => self.D = data,
            REG_E => self.E = data,
            REG_H => self.H = data,
            REG_L => self.L = data,
            _ => panic!("No such register exists: {:#010b}", reg),
        }
    }

    // CPU instructions
    // ----------------

    // All instructions return the number of clock cycles used.

    // 8-bit transfer and input/output instructions
    // --------------------------------------------

    // LD r,n : Loads 8-bit immediate data into register r.
    // 00 r 110
    fn ld_r_n(&mut self, opcode: u8) -> u8 {
        let register = (opcode & 0b00_111_000) >> 3;
        self.PC += 1;
        let immediate = self.mmu.read_byte(self.PC);
        self.set_register(register, immediate);
        self.PC += 1;
        2
    }

    // LD r, r' : Loads the contents of register r' into register r.
    // 01 r r'
    fn ld_r_rp(&mut self, opcode: u8) -> u8 {
        let data = self.get_register(opcode & 0b00_000_111);
        let register = (opcode & 0b00_111_000) >> 3;
        self.set_register(register, data);
        self.PC += 1;
        1
    }

    // LD r, (HL) : Loads the contents of memory (8 bits) specified by
    // register pair HL into register r.
    // 01 r 110
    fn ld_r_hl(&mut self, opcode: u8) -> u8 {
        let msb = self.get_register(REG_H);
        let lsb = self.get_register(REG_L);
        let location: u16 = pair(msb, lsb);
        let data = self.mmu.read_byte(location);
        let register = (opcode & 0b00_111_000) >> 3;
        self.set_register(register, data);
        self.PC += 1;
        2
    }

    // LD (HL), r : Store the contents of register r in memory specified by register pair HL.
    // 01 110 r
    fn ld_hl_r(&mut self, opcode: u8) -> u8 {
        let msb = self.get_register(REG_H);
        let lsb = self.get_register(REG_L);
        let location: u16 = pair(msb, lsb);
        let register = opcode & 0b00_000_111;
        let data = self.get_register(register);
        self.mmu.write_byte(location, data);
        self.PC += 1;
        2
    }

    // LD (HL), n : Loads 8-bit immediate data n into memory specified by register pair HL.
    // 00 110 110
    fn ld_hl_n(&mut self) -> u8 {
        let msb = self.get_register(REG_H);
        let lsb = self.get_register(REG_L);
        let location: u16 = pair(msb, lsb);
        self.PC += 1;
        let data = self.mmu.read_byte(self.PC);
        self.mmu.write_byte(location, data);
        self.PC += 1;
        3
    }

    // LD A, (BC) : Loads the contents specified by the contents of register pair BC into
    // register A.
    // 00 001 010
    fn ld_a_bc(&mut self) -> u8 {
        let msb = self.get_register(REG_B);
        let lsb = self.get_register(REG_C);
        let location: u16 = pair(msb, lsb);
        let data = self.mmu.read_byte(location);
        self.set_register(REG_A, data);
        self.PC += 1;
        2
    }

    // LD A, (DE) : Loads the contents specified by the contents of register pair DE into
    // register A.
    // 00 011 010
    fn ld_a_de(&mut self) -> u8 {
        let msb = self.get_register(REG_D);
        let lsb = self.get_register(REG_E);
        let location: u16 = pair(msb, lsb);
        let data = self.mmu.read_byte(location);
        self.set_register(REG_A, data);
        self.PC += 1;
        2
    }

    // LD A, (C) : Loads into register A the contents of the internal RAM, port register,
    // or mode register at the address in the range 0xFF00-0xFFFF specified by register C.
    // (E.g., C is the LSB; MSB = 0xFF.)
    // 11 110 010
    fn ld_a_c(&mut self) -> u8 {
        let msb = 0xff;
        let lsb = self.get_register(REG_C);
        let location: u16 = pair(msb, lsb);
        let data = self.mmu.read_byte(location);
        self.set_register(REG_A, data);
        self.PC += 1;
        2
    }

    // LD (C), A : Loads the contents of register A in the internal RAM, port register,
    // or mode register at the address in the range 0xFF00-0xFFFF specified by register C.
    // 11 100 010
    fn ld_c_a(&mut self) -> u8 {
        let msb = 0xff;
        let lsb = self.get_register(REG_C);
        let location: u16 = pair(msb, lsb);
        let data = self.get_register(REG_A);
        self.mmu.write_byte(location, data);
        self.PC += 1;
        2
    }

    // LD A, (n) : Loads into register A the contents of the internal RAM, port register,
    // or mode register at the address in the range 0xFF00-0xFFFF specified by the
    // 8-bit immediate operand n.
    // 11 110 000
    fn ld_a_n(&mut self) -> u8 {
        let msb = 0xff;
        self.PC += 1;
        let lsb = self.mmu.read_byte(self.PC);
        let location: u16 = pair(msb, lsb);
        let data = self.mmu.read_byte(location);
        self.set_register(REG_A, data);
        self.PC += 1;
        3
    }

    // LD (n), A : Loads the contents of register A to the internal RAM, port register,
    // or mode register at the address in the range 0xFF00-0xFFFF specified by the
    // 8-bit immediate operand n.
    // 11 100 000
    fn ld_n_a(&mut self) -> u8 {
        let msb = 0xff;
        self.PC += 1;
        let lsb = self.mmu.read_byte(self.PC);
        let location: u16 = pair(msb, lsb);
        let data = self.get_register(REG_A);
        self.mmu.write_byte(location, data);
        self.PC += 1;
        3
    }

    // LD A, (nn) : Loads into register A the contents of the internal RAM or register
    // specified by the 16-bit immediate operand nn.
    // 11 111 010
    fn ld_a_nn(&mut self) -> u8 {
        self.PC += 1;
        let location = self.mmu.read_word(self.PC);
        self.PC += 2;
        let data = self.mmu.read_byte(location);
        self.set_register(REG_A, data);
        4
    }

    // LD (nn), A : Loads the contents of register A to the internal RAM or register
    // specified by the 16-bit immediate operand nn.
    // 11 101 010
    fn ld_nn_a(&mut self) -> u8 {
        self.PC += 1;
        let location = self.mmu.read_word(self.PC);
        self.PC += 2;
        let data = self.get_register(REG_A);
        self.mmu.write_byte(location, data);
        4
    }

    // LD A, (HLI) : Loads in register A the contents of memory specified by the contents
    // of register pair HL and simultaneously increments the contents of HL.
    // 00 101 010
    fn ld_a_hli(&mut self) -> u8 {
        let msb = self.get_register(REG_H);
        let lsb = self.get_register(REG_L);
        let location: u16 = pair(msb, lsb);
        let data = self.mmu.read_byte(location);
        self.set_register(REG_A, data);
        self.set_register(REG_H, ((location + 1) >> 8) as u8);
        self.set_register(REG_L, ((location + 1) & 0xff) as u8);
        self.PC += 1;
        2
    }

    // LD A, (HLD) : Loads in register A the contents of memory specified by the contents
    // of register pair HL and simultaneously decrements the contents of HL.
    // 00 111 010
    fn ld_a_hld(&mut self) -> u8 {
        let msb = self.get_register(REG_H);
        let lsb = self.get_register(REG_L);
        let location: u16 = pair(msb, lsb);
        let data = self.mmu.read_byte(location);
        self.set_register(REG_A, data);
        self.set_register(REG_H, ((location - 1) >> 8) as u8);
        self.set_register(REG_L, ((location - 1) & 0xff) as u8);
        self.PC += 1;
        2
    }

    // LD (BC), A : Store the contents of register A in the memory specified by register pair BC.
    // 00 000 010
    fn ld_bc_a(&mut self) -> u8 {
        let msb = self.get_register(REG_B);
        let lsb = self.get_register(REG_C);
        let location: u16 = pair(msb, lsb);
        let data = self.get_register(REG_A);
        self.mmu.write_byte(location, data);
        self.PC += 1;
        2
    }

    // LD (DE), A : Store the contents of register A in the memory specified by register pair DE.
    // 00 010 010
    fn ld_de_a(&mut self) -> u8 {
        let msb = self.get_register(REG_D);
        let lsb = self.get_register(REG_E);
        let location: u16 = pair(msb, lsb);
        let data = self.get_register(REG_A);
        self.mmu.write_byte(location, data);
        self.PC += 1;
        2
    }

    // LD (HLI), A : Stores the contents of register A in the memory specified by register pair HL
    // and simultaneously increments the contents of HL.
    // 00 100 010
    fn ld_hli_a(&mut self) -> u8 {
        let msb = self.get_register(REG_H);
        let lsb = self.get_register(REG_L);
        let location: u16 = pair(msb, lsb);
        let data = self.get_register(REG_A);
        self.mmu.write_byte(location, data);
        self.set_register(REG_H, ((location + 1) >> 8) as u8);
        self.set_register(REG_L, ((location + 1) & 0xff) as u8);
        self.PC += 1;
        2
    }

    // LD (HLD), A : Stores the contents of register A in the memory specified by register pair HL
    // and simultaneously increments the contents of HL.
    // 00 110 010
    fn ld_hld_a(&mut self) -> u8 {
        let msb = self.get_register(REG_H);
        let lsb = self.get_register(REG_L);
        let location: u16 = pair(msb, lsb);
        let data = self.get_register(REG_A);
        self.mmu.write_byte(location, data);
        self.set_register(REG_H, ((location - 1) >> 8) as u8);
        self.set_register(REG_L, ((location - 1) & 0xff) as u8);
        self.PC += 1;
        2
    }

    // 16-bit transfer instructions
    // ----------------------------

    // LD dd, nn : Loads 2 bytes of immediate data to register pair nn.
    // Register pairs: BC: 00, DE: 01, HL: 10, SP: 11
    // 00 dd0 001
    fn ld_dd_nn(&mut self, opcode: u8) -> u8 {
        self.PC += 1;
        let data = self.mmu.read_word(self.PC);
        let reg_pair = (opcode & 0b00_110_000) >> 4;
        match reg_pair {
            0b00 => {
                self.set_register(REG_B, (data >> 8) as u8);
                self.set_register(REG_C, (data & 0xff) as u8);
            },
            0b01 => {
                self.set_register(REG_D, (data >> 8) as u8);
                self.set_register(REG_E, (data & 0xff) as u8);
            },
            0b10 => {
                self.set_register(REG_H, (data >> 8) as u8);
                self.set_register(REG_L, (data & 0xff) as u8);
            },
            0b11 => {
                self.SP = data;
            },
            _ => panic!("Unknown register pair code: {:#010b}", reg_pair)
        };
        self.PC += 2;
        3
    }

    // LD SP, HL : Loads the contents of register pair HL in stack pointer SP.
    // 11 111 001
    fn ld_sp_hl(&mut self) -> u8 {
        self.SP = ((self.H as u16) << 8) | ((self.L as u16) & 0xff);
        self.PC += 1;
        2
    }

    // PUSH qq : Pushes the contents of register pair qq onto the memory stack.
    // First 1 is subtracted from SP and the contents of the higher portion of qq
    // are placed on the stack. The the lower portion of qq are then placed on the stack.
    // The contents of SP are automatically decremented by 2.
    // Register pairs: BC: 00, DE: 01, HL: 10, AF: 11
    // 11 qq0 101
    fn push_qq(&mut self, opcode: u8) -> u8 {
        let reg_pair = (opcode & 0b00_110_000) >> 4;
        match reg_pair {
            0b00 => {
                self.mmu.write_byte(self.SP - 1, self.B);
                self.mmu.write_byte(self.SP - 2, self.C);
            },
            0b01 => {
                self.mmu.write_byte(self.SP - 1, self.D);
                self.mmu.write_byte(self.SP - 2, self.E);
            },
            0b10 => {
                self.mmu.write_byte(self.SP - 1, self.H);
                self.mmu.write_byte(self.SP - 2, self.L);
            },
            0b11 => {
                self.mmu.write_byte(self.SP - 1, self.A);
                self.mmu.write_byte(self.SP - 2, self.F);
            },
            _ => panic!("Unknown register pair code: {:#010b}", reg_pair)
        };
        self.SP -= 2;
        self.PC += 1;
        4
    }

    // POP qq : Pops contents from the memory stack and into register pair qq.
    // First the contents of memory specified by the contents of SP are loaded into
    // the lower portion of qq. Next, the contents of SP are incremented by 1 and the
    // contents of memory they specify are loaded into the upper portion of qq.
    // The contents of SP are automatically incremented by 2.
    // Register pairs: BC: 00, DE: 01, HL: 10, AF: 11
    // 11 qq0 001
    fn pop_qq(&mut self, opcode: u8) -> u8 {
        let reg_pair = (opcode & 0b00_110_000) >> 4;
        match reg_pair {
            0b00 => {
                let (low, high) = (self.mmu.read_byte(self.SP), self.mmu.read_byte(self.SP + 1));
                self.set_register(REG_C, low);
                self.set_register(REG_B, high);
            },
            0b01 => {
                let (low, high) = (self.mmu.read_byte(self.SP), self.mmu.read_byte(self.SP + 1));
                self.set_register(REG_E, low);
                self.set_register(REG_D, high);
            },
            0b10 => {
                let (low, high) = (self.mmu.read_byte(self.SP), self.mmu.read_byte(self.SP + 1));
                self.set_register(REG_L, low);
                self.set_register(REG_H, high);
            },
            0b11 => {
                let (low, high) = (self.mmu.read_byte(self.SP), self.mmu.read_byte(self.SP + 1));
                self.F = low;
                self.set_register(REG_A, high);
            },
            _ => panic!("Unknown register pair code: {:#010b}", reg_pair)
        };
        self.SP += 2;
        self.PC += 1;
        3
    }

    // LDHL SP, e : The 8-bit operand e is added to SP and the result is stored in HL.
    // e is in the range -128 to 127
    // Flags:
    // Z reset
    // H set if there is a carry from bit 11; otherwise reset
    // N reset
    // CY set if there is a carry from bit 15; otherwise reset
    // 11 111 000
    fn ldhl_sp_e(&mut self) -> u8 { // F is ZNHC0000
        self.PC += 1;
        let operand = self.mmu.read_byte(self.PC) as i8;
        3
    }

    // LD (nn), SP : Stores the lower byte of SP at address nn specified by the
    // 16-bit immediate operand nn and the upper byte of SP at address nn + 1.
    // 00 001 000
    fn ld_nn_sp(&mut self) -> u8 {
        //
        5
    }

    /// Decodes and executes instruction, then returns the number of clock cycles used.
    pub fn execute(&mut self, opcode: u8) -> u8 {
        match opcode {
            // 8-bit transfer and input/output instructions
            // --------------------------------------------
            0x6 | 0xe | 0x16 | 0x1e | 0x26 | 0x2e | 0x3e => self.ld_r_n(opcode),

            0x40...0x45 | 0x47...0x4d | 0x4f...0x55 | 0x57...0x5d | 0x5f...0x65 | 0x67...0x6d
            | 0x6f | 0x78...0x7d | 0x7f => self.ld_r_rp(opcode),

            0x46 | 0x4e | 0x56 | 0x5e | 0x66 | 0x6e | 0x7e => self.ld_r_hl(opcode),

            0x70...0x75 | 0x77 => self.ld_hl_r(opcode),

            0b00_110_110 => self.ld_hl_n(),

            0b00_001_010 => self.ld_a_bc(),

            0b00_011_010 => self.ld_a_de(),

            0b11_110_010 => self.ld_a_c(),

            0b11_100_010 => self.ld_c_a(),

            0b11_110_000 => self.ld_a_n(),

            0b11_100_000 => self.ld_n_a(),

            0b11_111_010 => self.ld_a_nn(),

            0b11_101_010 => self.ld_nn_a(),

            0b00_101_010 => self.ld_a_hli(),

            0b00_111_010 => self.ld_a_hld(),

            0b00_000_010 => self.ld_bc_a(),

            0b00_010_010 => self.ld_de_a(),

            0b00_100_010 => self.ld_hli_a(),

            0b00_110_010 => self.ld_hld_a(),

            // 16-bit transfer instructions
            // ----------------------------
            0b00_000_001 | 0b00_010_001 | 0b00_100_001 | 0b00_110_001 => self.ld_dd_nn(opcode),

            0b11_111_001 => self.ld_sp_hl(),

            0b11_000_101 | 0b11_010_101 | 0b11_100_101 | 0b11_110_101 => self.push_qq(opcode),

            0b11_000_001 | 0b11_010_001 | 0b11_100_001 | 0b11_110_001 => self.pop_qq(opcode),

            0b11_111_000 => self.ldhl_sp_e(),

            0b00_001_000 => self.ld_nn_sp(),

            _ => panic!("unrecognized opcode: {:#010b}", opcode)
        }
    }
}

fn pair(msb: u8, lsb: u8) -> u16 {
    ((msb as u16) << 8) | ((lsb as u16) & 0xff)
}